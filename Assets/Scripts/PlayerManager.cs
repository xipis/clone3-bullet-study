using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public float MoveSpeed;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        UpdateMove();
        UpdateRotate();
    }

    private void UpdateMove()
    {
        //(0,0)
        Vector2 dir = Vector2.zero;
        if (Input.GetKey(KeyCode.A))
        {
            dir += new Vector2(-1, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            dir += new Vector2(1, 0);
        }
        if (Input.GetKey(KeyCode.W))
        {
            dir += new Vector2(0, 1);
        }

        if (Input.GetKey(KeyCode.S)) {
            dir += new Vector2(0, -1);
        }

        var oldPos = transform.position;
        Vector2 moveDis = dir * MoveSpeed * Time.deltaTime;
        oldPos.x += moveDis.x;
        oldPos.y += moveDis.y;
        transform.position = oldPos;
    }

    private void UpdateRotate()
    {
        var mousePos = Input.mousePosition;
        var screenPos = Camera.main.WorldToScreenPoint(transform.position);
        screenPos.z = 0;
        mousePos.z = 0;
        var dir = mousePos - screenPos;
        transform.rotation = Quaternion.FromToRotation(Vector3.up, dir.normalized);
    }
}
